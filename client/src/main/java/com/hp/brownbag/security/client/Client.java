package com.hp.brownbag.security.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Christophe Weyn
 */
public class Client implements Runnable {

	@Autowired
	private BrownBagWsc brownBagWsc;

	public static void main(String args[]) {

		System.setProperty("javax.net.ssl.trustStore", "certificates/client/truststore.jks");
		System.setProperty("javax.net.ssl.trustStorePassword", "changeit");

		System.setProperty("javax.net.ssl.keyStore", "certificates/client/keystore.jks");
		System.setProperty("javax.net.ssl.keyStorePassword", "changeit");

		ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("spring/spring.ctx.xml");

		Client client = new Client();
		context.getBeanFactory().autowireBeanProperties(client, AutowireCapableBeanFactory.AUTOWIRE_NO, false);


		client.run();
	}

	public void run() {
		brownBagWsc.receiveHelloFromServer();
	}
}
