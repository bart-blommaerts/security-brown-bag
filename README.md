# This is my README


Server:

Create a keystore: keytool -genkey -keyalg RSA -alias sec_server -keystore keystore.jks -storepass changeit -validity 360 -keysize 2048

extract certificaat from keystore: keytool -export -keystore keystore.jks -alias sec_server -file sec_server.cer



Client
Put server certificate in truststore
keytool -import -file sec_server.cer -alias sec_server -keystore truststore.jks

Create a keystore: keytool -genkey -keyalg RSA -alias sec_client -keystore keystore.jks -storepass changeit -validity 360 -keysize 2048