package com.hp.brownbag.security.server;

import com.sun.net.httpserver.HttpsConfigurator;
import com.sun.net.httpserver.HttpsParameters;
import com.sun.net.httpserver.HttpsServer;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.TrustManagerFactory;
import javax.xml.ws.Endpoint;
import javax.xml.ws.soap.SOAPBinding;
import java.io.FileInputStream;
import java.net.InetSocketAddress;
import java.security.KeyStore;
import java.security.SecureRandom;

/**
 * @author Christophe Weyn
 */
public class BrownBagServicePublisher {

    private static final String HOSTNAME = "server.security.brownbag.hp.com";

    private static final String KEY_STORE = "certificates/server/keystore.jks";
    private static final String TRUST_STORE = "certificates/server/truststore.jks";
    private static final String KEY_PASS = "changeit";


    public static void main(String[] args) throws Exception {
        System.out.println("Publish the service");


        BrownBagServicePublisher publisher = new BrownBagServicePublisher();

        publisher.createHttps();
        //		publisher.createHttp();


    }

    private void createHttp() throws Exception {
        Endpoint ep = Endpoint.publish("http://" + HOSTNAME, new BrownBagServiceImpl());
        SOAPBinding binding = (SOAPBinding) ep.getBinding();
        binding.setMTOMEnabled(true);
    }


    private void createHttps() throws Exception {
        //Load the keystore
        KeyManagerFactory keyFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        KeyStore keyStore = KeyStore.getInstance("JKS");
        keyStore.load(new FileInputStream(KEY_STORE), KEY_PASS.toCharArray());
        keyFactory.init(keyStore, KEY_PASS.toCharArray());

        //Load the truststore
        TrustManagerFactory trustFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        KeyStore trustStore = KeyStore.getInstance("JKS");
        trustStore.load(new FileInputStream(TRUST_STORE), KEY_PASS.toCharArray());
        trustFactory.init(trustStore);

        //Create the SSL context with keystore & truststore
        SSLContext ssl = SSLContext.getInstance("TLS");
        ssl.init(keyFactory.getKeyManagers(), trustFactory.getTrustManagers(), new SecureRandom());

        HttpsServer httpsServer = HttpsServer.create(new InetSocketAddress(HOSTNAME, 8443), 10);
        httpsServer.setHttpsConfigurator(new HttpsConfigurator(ssl) {

            public void configure(HttpsParameters params) {
                //require client authentication
                SSLParameters sslparams = getSSLContext().getDefaultSSLParameters();
                sslparams.setNeedClientAuth(true);
                params.setSSLParameters(sslparams);
            }
        });

        httpsServer.start();

        Endpoint endpoint = Endpoint.create(new BrownBagServiceImpl());

        endpoint.publish(httpsServer.createContext("/BrownBagService"));
    }

	 /*
    Error:
	Caused by: sun.security.validator.ValidatorException: PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException: unable to find valid certification path to requested target

	  Oorzaak een certificaat dat niet getrust wordt, geen ceritifcaat, certificaat vervallen, certificaat revoked.

	*/
}